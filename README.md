# StarryNight

WPF .NET application with animations. Application use files from AnimatedBee
Application is based on book "Head First C#" O'REILLY Jennifer Greene, Andrew Stellman

## Purpose

The purpose of this application is to learn more about animations.

## Author

* **Patryk Kupis** - [Linkedin](https://www.linkedin.com/in/patryk-kupis-12a453162/), [Gitlab](https://gitlab.com/Kupis)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details