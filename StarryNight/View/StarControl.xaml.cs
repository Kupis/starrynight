﻿using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace StarryNight.View
{
    /// <summary>
    /// Interaction logic for StarControl.xaml
    /// </summary>
    public partial class StarControl : UserControl
    {
        public StarControl()
        {
            InitializeComponent();
        }

        public void FadeIn()
        {
            Storyboard fadeInStoryboard = FindResource("fadeInStoryboard") as Storyboard;
            fadeInStoryboard.Begin();
        }

        public void FadeOut()
        {
            Storyboard fadeOutStoryboard = FindResource("fadeOutStoryboard") as Storyboard;
            fadeOutStoryboard.Begin();
        }
    }
}
